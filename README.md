# dynmap-skyblock #

This badly-named plugin adds [dynmap][] markers for [uSkyBlock][uskyblock] islands.

[dynmap]: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1286593-dynmap-dynamic-web-based-maps-for-minecraft
[uskyblock]: http://dev.bukkit.org/bukkit-plugins/ultimate-skyblock/

## Requirements and Installation ##

This plugin depends on dynmap. It oviously also requires uSkyBlock (2.0) to work. Install all these plugins and start your server once. Then shut it down. The configuration file for dynmap-skyblock should have been generated.

Here is a commented config.yml that shows you how to configure dynmap-skyblock for your server.


```yaml
world: skyworld         # the name of the skyblock world
islandGroundHeight: 125 # the height of the islands, you may just keep this value
markerIcon: redflag     # the icon used for the island markers
ignoredIslands:         # a list of ignored islands
- 0,0                   # here, island 0,0 (which is 'islands/0,0.yml') won't get a marker
```

A list of all marker icons is available [here][marker-icons].

[marker-icons]: https://github.com/webbukkit/dynmap/wiki/Using-markers#marker-icons