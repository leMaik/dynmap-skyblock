package de.craften.plugins.dynmapskyblock;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerSet;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A plugin that adds dynmap markers for uSkyBlock islands.
 *
 * @author Maik Marschner (leMaik)
 */
public class DynmapSkyblockPlugin extends JavaPlugin {
    private static final Pattern islandFileNamePattern = Pattern.compile("(-?\\d+),(-?\\d+)\\.yml");
    private File skyblockDirectory;
    private MarkerAPI markerApi;
    private MarkerSet markers;
    private Map<String, Marker> islands = new HashMap<>();
    private Queue<Island> islandsToAdd = new ConcurrentLinkedQueue<>();
    private Set<Point> ignoredIslands = new HashSet<>();

    @Override
    public void onEnable() {
        saveDefaultConfig();

        skyblockDirectory = new File(new File(getDataFolder().getParentFile(), "uSkyBlock"), "islands");

        for (String island : getConfig().getStringList("ignoredIslands")) {
            String[] p = island.split(",");
            ignoredIslands.add(new Point(Integer.parseInt(p[0]), Integer.parseInt(p[1])));
        }
        getLogger().info(ignoredIslands.size() + " ignored islands");

        Plugin dynmap = getServer().getPluginManager().getPlugin("dynmap");
        markerApi = ((DynmapAPI) dynmap).getMarkerAPI();

        markers = markerApi.getMarkerSet("dynmapskyblock.markerset");
        if (markers == null) {
            markers = markerApi.createMarkerSet("dynmapskyblock.markerset", getConfig().getString("markerLabel"), null, false);
        } else {
            markers.setMarkerSetLabel(getConfig().getString("markerLabel"));
        }
        markers.setHideByDefault(false);

        getServer().getScheduler().runTaskTimerAsynchronously(this, new IslandReader(), 0, 20 * 60 * 60); //60 min
        getServer().getScheduler().runTaskTimerAsynchronously(this, new SkyblockMarkerUpdater(), 20 * 60, 20 * 60 * 30); //30min
    }

    private class SkyblockMarkerUpdater implements Runnable {
        @Override
        public void run() {
            Island island;
            while ((island = islandsToAdd.poll()) != null) {
                if (!ignoredIslands.contains(island.getPosition())) { //TODO doesn't work?!
                    Marker m = islands.get(island.getId());
                    if (m != null)
                        m.deleteMarker();

                    m = markers.createMarker(island.getId(), "Island by " + island.getLeader(),
                            false, getConfig().getString("world"), island.getX(), getConfig().getInt("islandGroundHeight"), island.getZ(),
                            markerApi.getMarkerIcon(getConfig().getString("markerIcon")), false);

                    if (m != null)
                        islands.put(island.getId(), m);
                }
            }
        }
    }

    private class IslandReader implements Runnable {
        @Override
        public void run() {
            if (skyblockDirectory == null || !skyblockDirectory.isDirectory())
                return;
            File[] files = skyblockDirectory.listFiles();
            if (files == null)
                return;

            for (final File island : files) {
                getServer().getScheduler().runTaskAsynchronously(DynmapSkyblockPlugin.this, new Runnable() {
                    @Override
                    public void run() {
                        Matcher matcher = islandFileNamePattern.matcher(island.getName());
                        if (matcher.matches()) {
                            ConfigurationSection c = YamlConfiguration.loadConfiguration(island);
                            islandsToAdd.add(new Island(
                                    c.getString("party.leader"),
                                    Integer.parseInt(matcher.group(1)),
                                    Integer.parseInt(matcher.group(2))
                            ));
                        }
                    }
                });
            }
        }
    }
}
