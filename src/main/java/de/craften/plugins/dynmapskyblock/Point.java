package de.craften.plugins.dynmapskyblock;

/**
 * A two-dimensional point with an x- and a z-coordinate.
 *
 * @author Maik Marschner (leMaik)
 */
public class Point {
    private int x;
    private int z;

    public Point(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public int hashCode() {
        return x + 13 * z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point other = (Point) obj;
            return other.x == x && other.z == z;
        }
        return false;
    }
}
