package de.craften.plugins.dynmapskyblock;

/**
 * An uSkyBlock island.
 *
 * @author Maik Marschner (leMaik)
 */
public class Island {
    private String leader;
    private Point position;

    public Island(String leader, int x, int z) {
        this.leader = leader;
        this.position = new Point(x, z);
    }

    public String getLeader() {
        return leader;
    }

    public double getX() {
        return position.getX();
    }

    public double getZ() {
        return position.getZ();
    }

    public String getId() {
        return "island-" + position.getX() + '-' + position.getZ();
    }

    public Point getPosition() {
        return position;
    }
}
